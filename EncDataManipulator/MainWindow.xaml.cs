﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using SwShEncounters.Data;
using SwShEncounters.Models;

namespace EncDataManipulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            (Resources["Areas"] as CollectionViewSource).Source = new ObservableCollection<Area>(EncounterData.SwordEncounters);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog()
            {
                AddExtension = true,
                CheckPathExists = true,
                Filter = "C# Source Files (*.cs)|*.cs|All Files|*.*",
                FilterIndex = 0,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                OverwritePrompt = true,
                Title = "Data File"
            };
            if (!(sfd.ShowDialog() ?? false)) return;

            if (!((Resources["Areas"] as CollectionViewSource)?.Source is ObservableCollection<Area> areas)) return;

            const string tab = "    ";
            var prepend = string.Empty;
            var sb = new StringBuilder();
            sb.Append(prepend).AppendLine("List<Area> encounters = new List<Area>()");
            sb.Append(prepend).Append("{");
            var firstArea = true;
            prepend += tab;
            foreach (var area in areas)
            {
                if (!firstArea) sb.Append(",");
                sb.AppendLine();
                sb.Append(prepend).AppendLine("new Area()");
                sb.Append(prepend).AppendLine("{");
                prepend += tab;
                sb.Append(prepend).AppendLine($"Name = \"{area.Name.CleanString()}\",");
                sb.Append(prepend).AppendLine("EncounterTables = new List<EncounterListing>()");
                sb.Append(prepend).Append("{");
                prepend += tab;
                var firstTable = true;
                foreach (var table in area.EncounterTables)
                {
                    if (!firstTable) sb.Append(",");
                    sb.AppendLine();
                    sb.Append(prepend).AppendLine("new EncounterListing()");
                    sb.Append(prepend).AppendLine("{");
                    prepend += tab;
                    sb.Append(prepend).AppendLine($"Type = EncounterType.{table.Type},");
                    sb.Append(prepend).AppendLine($"Weather = Weather.{table.Weather},");
                    sb.Append(prepend).AppendLine($"MinimumLevel = {table.MinimumLevel},");
                    sb.Append(prepend).AppendLine($"MaximumLevel = {table.MaximumLevel},");
                    sb.Append(prepend).AppendLine("Pokemon = new List<Pokemon>()");
                    sb.Append(prepend).Append("{");
                    prepend += tab;
                    var firstPokemon = true;
                    foreach (var pokemon in table.Pokemon)
                    {
                        if (!firstPokemon) sb.Append(",");
                        sb.AppendLine();
                        sb.Append(prepend).Append($"new Pokemon() {{ Name =\"{pokemon.Name.CleanString()}\", Rate = {pokemon.Rate} }}");
                        firstPokemon = false;
                    }
                    sb.AppendLine();
                    prepend = prepend.Substring(0, prepend.Length - tab.Length);
                    sb.Append(prepend).AppendLine("}");
                    prepend = prepend.Substring(0, prepend.Length - tab.Length);
                    sb.Append(prepend).Append("}");
                    firstTable = false;
                }
                sb.AppendLine();
                prepend = prepend.Substring(0, prepend.Length - tab.Length);
                sb.Append(prepend).AppendLine("}");
                prepend = prepend.Substring(0, prepend.Length - tab.Length);
                sb.Append(prepend).Append("}");
                firstArea = false;
            }
            sb.AppendLine();
            prepend = prepend.Substring(0, prepend.Length - tab.Length);
            sb.Append(prepend).Append("};");

            if (System.IO.File.Exists(sfd.FileName)) System.IO.File.Delete(sfd.FileName);
            System.IO.File.WriteAllText(sfd.FileName, sb.ToString());
        }
    }

    public static class Extensions
    {
        public static string CleanString(this string value) => value.Replace("\\", "\\\\").Replace("\"", "\\\"");
    }
}
