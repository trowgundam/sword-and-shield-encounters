﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwShEncounters.Models;

namespace SwShEncounters.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(Game? game = null)
        {
            // List<Area> areas = await Task.Run(() => System.Text.Json.JsonSerializer.Deserialize<List<Area>>(EncounterData));
            if (game == null)
            {
                if (Request.Cookies.ContainsKey("Game") && Enum.TryParse<Game>(Request.Cookies["Game"], out var g))
                {
                    game = g;
                    // Refresh the cookie.
                    Response.Cookies.Append("Game", game.Value.ToString(), new Microsoft.AspNetCore.Http.CookieOptions() { Expires = new DateTimeOffset(DateTime.Now.AddDays(7)), SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Strict });
                }
                else
                {
                    game = Game.Sword;
                    if (Request.Cookies.ContainsKey("Game")) Response.Cookies.Delete("Game");    // The cookie had an invalid value, so get rid of it.
                }
            }
            else
            {
                // Set the cookie
                Response.Cookies.Append("Game", game.Value.ToString(), new Microsoft.AspNetCore.Http.CookieOptions() { Expires = new DateTimeOffset(DateTime.Now.AddDays(7)), SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Strict });
            }

            ViewData["Game"] = game.Value;
            var data = game.Value switch
            {
                Game.Sword => Data.EncounterData.SwordEncounters,
                Game.Shield => Data.EncounterData.ShieldEncounters,
                _ => Data.EncounterData.SwordEncounters,
            };
            data.Sort(new Comparison<Area>((a1, a2) => a1.Name.CompareTo(a2.Name)));
            return View(data);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
