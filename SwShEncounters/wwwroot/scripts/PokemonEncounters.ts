document.getElementById('pokemon-filter').addEventListener("input", filterChange);

function filterChange(ev: Event) {
    let filter = <HTMLInputElement>ev.target;
    let clearFilter = <HTMLButtonElement>document.getElementById('clear-filter');
    clearFilter.disabled = filter.value.length === 0;
}

function filterPokemon() {
    let pokemonFilter = <HTMLInputElement>document.getElementById('pokemon-filter');
    let filter = pokemonFilter.value;
    if (filter) {
        if (filter.length === 0) return;

        let areas = document.querySelectorAll('div[data-level="area"]');
        areas.forEach((element) => {
            let area = <HTMLDivElement>element;
            let weatherCount = parseInt(area.dataset.weatherCount); 

            if (weatherCount == 1) {
                let encounterTable = area.children[1].querySelector("table");
                area.hidden = filterWeather(encounterTable, filter) <= 0;
            } else {
                let weathers = area.children[1].children[0].children;
                let hidden = 0;
                for (var i = 0; i < weathers.length; i++) {
                    let weather = <HTMLDivElement>weathers[i];
                    let encounterTable = weather.querySelector("table");
                    weather.hidden = filterWeather(encounterTable, filter) <= 0;
                    if (weather.hidden) hidden++;
                }
                area.hidden = hidden === weathers.length;
            }
        });
    }
}

function filterWeather(encounterTable: HTMLTableElement, filter: string): number {
    for (var i = 0; i < encounterTable.children.length; i++) {
        let element = <HTMLTableSectionElement>encounterTable.children[i];
        var header: HTMLTableSectionElement;

        if (!('info' in element.dataset)) {
            if (element.tagName == 'THEAD') {
                header = element;
            } else if (element.tagName == 'TBODY') {
                let hiddenRows = 0;

                for (var r = 0; r < element.rows.length; r++) {
                    var pokemon = element.rows[r].dataset.pokemon;
                    element.rows[r].hidden = pokemon.toLowerCase().indexOf(filter.toLowerCase()) == -1;
                    if (element.rows[r].hidden) {
                        hiddenRows++;
                    }
                }

                element.hidden = hiddenRows == element.rows.length;
                header.hidden = element.hidden;
            }
        }
    }

    let visible = 0;
    for (var i = 0; i < encounterTable.children.length; i++) {
        let s = <HTMLTableSectionElement>encounterTable.children[i];
        if (!s.hidden) visible++;
    }

    return visible - 1;
}

function clearFilter() {
    let pokemonFilter = <HTMLInputElement>document.getElementById('pokemon-filter');

    document.querySelectorAll('div[data-level="area"]').forEach(
        (element) => {
            let area = <HTMLDivElement>element;
            area.querySelectorAll('table').forEach(
                (table) => {
                    for (var s = 0; s < table.children.length; s++) {
                        let section = <HTMLTableSectionElement>table.children[s];
                        for (var r = 0; r < section.rows.length; r++) {
                            let row = <HTMLTableRowElement>section.rows[r];
                            row.hidden = false;
                        }
                        section.hidden = false;
                    }
                    table.hidden = false;
                });

            let weatherCount = parseInt(area.dataset.weatherCount);
            if (weatherCount > 1) {
                let weathers = area.children[1].children[0].children;
                for (var i = 0; i < weathers.length; i++) {
                    let weather = <HTMLDivElement>weathers[i];
                    weather.hidden = false;
                }
            }

            area.hidden = false;
        });

    pokemonFilter.value = "";
}