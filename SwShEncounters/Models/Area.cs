﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwShEncounters.Models
{
    public class Area
    {
        public string Name { get; set; }
        public List<EncounterListing> EncounterTables { get; set; }
    }
}
