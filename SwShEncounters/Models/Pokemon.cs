﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwShEncounters.Models
{
    public class Pokemon
    {
        public string Name { get; set; }
        public byte Rate { get; set; }
    }
}
