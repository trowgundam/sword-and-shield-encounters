﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SwShEncounters.Models
{
    public enum Game
    {
        Sword,
        Shield
    }

    public enum EncounterType
    {
        Overworld,
        Random,
        Fishing,
        Surfing,
        [Description("Shaking Trees")]
        ShakingTrees,
        Ground,
        Flying,
        Garbage
    }

    public enum Weather
    {
        [Icon("seasons")]
        [Description("All Weather")]
        All = 0,
        [Icon("cloud")]
        Normal,
        [Icon("cloud-1")]
        Overcast,
        [Icon("rain")]
        Raining,
        [Icon("rain-1")]
        Thunderstorm,
        [Icon("sun")]
        [Description("Intense Sun")]
        IntenseSun,
        [Icon("christmas")]
        Snowing,
        [Icon("snow")]
        Snowstorm,
        [Icon("sandstorm")]
        Sandstorm,
        [Icon("foggy")]
        [Description("Heavy Fog")]
        HeavyFog
    }
}
