﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwShEncounters.Models
{
    public class EncounterListing
    {
        public EncounterType Type { get; set; }
        public Weather Weather { get; set; }
        public List<Pokemon> Pokemon { get; set; }
        public byte MinimumLevel { get; set; }
        public byte MaximumLevel { get; set; }
    }
}
