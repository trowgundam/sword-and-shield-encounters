﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SwShEncounters
{
    public static class Utilities
    {
        public static string GetDescription<T>(this T enumValue) where T : Enum =>
            typeof(T).GetField(enumValue.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false)?.Cast<DescriptionAttribute>().FirstOrDefault()?.Description ?? enumValue.ToString();
        public static string GetIconId<T>(this T enumValue) where T : Enum =>
            typeof(T).GetField(enumValue.ToString()).GetCustomAttributes(typeof(IconAttribute), false)?.Cast<IconAttribute>().FirstOrDefault()?.IconId;
        public static string GetSerbiiLink(this string pokemon)
        {
            var match = System.Text.RegularExpressions.Regex.Match(pokemon, "^(.+?)(?:-\\d+)$");
            if (match.Success) pokemon = match.Groups[1].Value;
            return $"https://www.serebii.net/pokedex-swsh/{Uri.EscapeUriString(pokemon.ToLower().Replace(" ", ""))}/";
        }
    }

    public class IconAttribute : Attribute
    {
        public string IconId { get; }
        public IconAttribute(string IconId) => this.IconId = IconId;
    }
}
